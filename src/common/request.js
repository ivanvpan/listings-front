var $ = require('jquery');

var config = require('common/config');

var request = {
    get: function (url, callback) {
        $.ajax({
            dataType: 'json',
            url: config.backendHost + url,
            success: function (data, status, req) {
                if (data.status == 'error') {
                    callback(data.message, null);
                } else {
                    callback(null, data.data);
                }
            },
            error: function (req, status, error) {
                callback(error, null);
            }
        });
	},
	post: function (url, postData, callback) {
        $.ajax({
			type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
			data: JSON.stringify(postData),
            url: config.backendHost + url,
            success: function (data, status, req) {
                if (data.status == 'error') {
                    callback(data.message, null);
                } else {
                    callback(null, data.data);
                }
            },
            error: function (req, status, error) {
                callback(error, null);
            }
        });
	}
};

module.exports = request;
