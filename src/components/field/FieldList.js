var React = require('react/addons');
//var Link = require('react-router').Link;
//var b = require('react-bootstrap');

var request = require('common/request');

//var Actions = require('actions/xxx')

var Component = React.createClass({
	getInitialState: function() {
		return {fields: []};
	},
	componentDidMount: function () {
		request.get('/api/v1/field', function (err, data) {
			if (err) {
				console.error(err);
				return;
			}

			console.debug('FieldList got', data);
			this.setState({
				fields: data
			});
		}.bind(this));
	},
	render: function () {
		var fields;
		if (this.state.fields) {
			fields = this.state.fields.map(function (bp) {
				return (
					<tr key={bp.id}>
						<td><a href="#">{bp.name}</a></td>
					</tr>
				);
			});
		}

		return (
			<div className='FieldList'>
				<table className="table table-striped">
					<thead>
						<tr>
							<th>Name</th>
						</tr>
					</thead>
					<tbody>
						{fields}
					</tbody>
				</table>
			</div>
		);
	}
});

module.exports = Component;
