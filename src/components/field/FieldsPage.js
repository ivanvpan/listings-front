var React = require('react');

var FieldCreate = require('components/field/FieldCreate');
var FieldList = require('components/field/FieldList');

//require('styles/Documents.styl');

var Component = React.createClass({
	render: function () {
		return (
			<div className='Fields'>
				<FieldCreate/>
				<FieldList/>
			</div>
		);
	}
});

module.exports = Component;
