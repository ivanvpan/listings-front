var React = require('react');
var DropdownList = require('react-widgets').DropdownList;
var request = require('common/request');
var classNames = require('classnames');

var Component = React.createClass({
	getInitialState: function() {
		return {
			isNameValid: true,
			isFormValid: false,
		};
	},
	validate: function () {
		// Name input
		var nameEl = React.findDOMNode(this.refs.name);
		var valid = true;

		if (!nameEl.value) {
			this.setState({
				isNameValid: false
			});
			valid = false;
		} else {
			this.setState({
				isNameValid: true
			});
		}
		this.setState({
			isFormValid: valid
		});
		return valid;
	},
	onInputChange: function (e) {
		this.validate();
	},
	onCancelClick: function (e) {
		e.preventDefault();
		this.props.closeForm();
	},
	onCreateClick: function (e) {
		e.preventDefault();
		var name = React.findDOMNode(this.refs.name).value;
		var valid = this.validate();
		var self = this;
		if (!valid) {
			return;
		}
		request.post('/api/v1/field', {
			name: name
		}, function (err, data) {
			if (err) {
				console.error(err);
			}
			console.debug('Saved');
			self.props.closeForm();
		});
	},
	render: function () {
		var nameGroupClasses = classNames({
			'form-group': true,
			'has-error': !this.state.isNameValid
		});
		var createButtonClasses = classNames({
			'btn': true,
			'btn-primary': true,
			'disabled': !this.state.isFormValid
		});

		return (
			<div className="FieldCreateForm">
				<form>
					<div className={nameGroupClasses}>
						<label className="control-label" for="name">Name *</label>
						<input onChange={this.onInputChange} className="form-control" placeholder="Name" type="text" name="name" ref="name"/>
					</div>

					<label className="checkbox" for="required">
						<label>
							<input className="custom-checkbox" type="checkbox" name="required" ref="required"/>
							Required
						</label>
					</label>

					<div className="form-group">
						<label className="control-label" for="values">Values (comma separated)</label>
						<input className="form-control" placeholder="Values" type="text" name="values" ref="values"/>
					</div>

					<div className="form-group">
						<label className="control-label" for="values">Default value</label>
						<input className="form-control" placeholder="Default value" type="text" name="default" ref="default"/>
					</div>

					{/* TODO Make a select2 option */}
					<div className="form-group">
						<label className="control-label" for="type">Field Type</label>
						<DropdownList/>
					</div>

					<button onClick={this.onCreateClick} className={createButtonClasses}>
						Create
					</button>
					&nbsp;
					<button onClick={this.onCancelClick} className="btn btn-default cancel">
						Cancel
					</button>
				</form>
			</div>
		);
	}
});

module.exports = Component;
