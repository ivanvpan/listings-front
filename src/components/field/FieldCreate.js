var React = require('react');
var classNames = require('classnames');

var FieldCreateForm = require('components/field/FieldCreateForm');

var Component = React.createClass({
	getInitialState: function() {
		return {
			isFormOpen: false,
		};
	},
	onNewClick: function (e) {
		e.preventDefault();
		this.setState({
			isFormOpen: true
		});
	},
	closeForm: function () {
		this.setState({
			isFormOpen: false
		});
	},
	render: function () {
		var buttonClass = classNames({
			btn: true,
			'btn-primary': true,
			hide: this.state.isFormOpen
		});
		return (
			<div className="FieldCreate">
				<button onClick={this.onNewClick} className={buttonClass}>
					+ New
				</button>
				{this.state.isFormOpen ? <FieldCreateForm closeForm={this.closeForm} /> : ''}
			</div>
		);
	}
});

module.exports = Component;
