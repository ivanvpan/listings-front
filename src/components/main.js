var React = require('react');
var Router = require('react-router');
var Route = Router.Route;

var App = require('./App');
var BlueprintsPage = require('components/blueprint/BlueprintsPage');
var DocumentsPage = require('components/document/DocumentsPage');
var FieldsPage = require('components/field/FieldsPage');

window.jQuery = window.$ = require('jquery');
require('bootstrap');

// CSS
require('normalize.css');
require('bootstrap/less/bootstrap.less');
require('react-widgets/dist/css/react-widgets.css');
//require('flat-ui/dist/css/flat-ui.css');
require('../styles/main.css');


console.info('Starting app');

var content = document.getElementById('content');

var routes = (
    <Route handler={App}>
        <Route name="blueprints" handler={BlueprintsPage}/>
        <Route name="documents" handler={DocumentsPage}/>
        <Route name="fields" handler={FieldsPage}/>
    </Route>
);

console.info('Running routes');
Router.run(routes, function (Root) {
    React.render(<Root/>, content);
});
