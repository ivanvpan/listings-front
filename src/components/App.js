var React = require('react');
var RouteHandler = require('react-router').RouteHandler;
//var ReactTransitionGroup = React.addons.TransitionGroup;
//var b = require('react-bootstrap');

var NavBar = require('components/NavBar');

var App = React.createClass({
	render: function() {
		console.debug('Rendering App');
		return (
			<div className="app">
				<NavBar/>
				<div className="main-content">
					<div className="container">
						<RouteHandler/>
					</div>
				</div>
			</div>
		);
	}
});

module.exports = App;
