var React = require('react/addons');
var Router = require('react-router');
var Link = Router.Link;
var b = require('react-bootstrap');

//var Actions = require('actions/xxx')

require('styles/NavBar.styl');

var NavBar = React.createClass({
    render: function () {
        console.debug('Rendering NavBar');
        return (
            <div className='NavBar'>
                <b.Navbar brand={<a href="#/">Listings</a>}>
                    <b.Nav>
                        <li><Link to="blueprints">Blueprints</Link></li>
                        <li><Link to="documents">Documents</Link></li>
                        <li><Link to="fields">Fields</Link></li>
                    </b.Nav>
                </b.Navbar>
            </div>
        );
    }
});

module.exports = NavBar;
