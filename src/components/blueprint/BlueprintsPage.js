var React = require('react/addons');

//var Actions = require('actions/xxx')

var BlueprintCreate = require('components/blueprint/BlueprintCreate');
var BlueprintList = require('components/blueprint/BlueprintList');

//require('styles/BlueprintList.styl');

var BlueprintsPage = React.createClass({
  render: function () {
    return (
        <div className='Blueprints'>
            <BlueprintCreate/>
            <BlueprintList/>
        </div>
      );
  }
});

module.exports = BlueprintsPage;
