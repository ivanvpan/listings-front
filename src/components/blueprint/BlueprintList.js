var React = require('react/addons');
//var Link = require('react-router').Link;
//var b = require('react-bootstrap');

var request = require('common/request');

//var Actions = require('actions/xxx')

var Component = React.createClass({
	getInitialState: function() {
		return {blueprints: []};
	},
	componentDidMount: function () {
		request.get('/api/v1/blueprint', function (err, data) {
			if (err) {
				console.error(err);
				return;
			}

			console.debug('BlueprintList got', data);
			this.setState({
				blueprints: data
			});
		}.bind(this));
	},
	render: function () {
		var blueprints;
		if (this.state.blueprints) {
			blueprints = this.state.blueprints.map(function (bp) {
				return (
					<tr key={bp.id}>
						<td><a href="#">{bp.name}</a></td>
					</tr>
				);
			});
		}

		return (
			<div className='BlueprintList'>
				<table className="table table-striped">
					<thead>
						<tr>
							<th>Name</th>
						</tr>
					</thead>
					<tbody>
						{blueprints}
					</tbody>
				</table>
			</div>
		);
	}
});

module.exports = Component;
