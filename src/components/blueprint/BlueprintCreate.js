var React = require('react');
var classNames = require('classnames');

var BlueprintCreateForm = require('components/blueprint/BlueprintCreateForm');

var Component = React.createClass({
	getInitialState: function() {
		return {
			isFormOpen: false,
		};
	},
	onNewClick: function (e) {
		e.preventDefault();
		this.setState({
			isFormOpen: true
		});
	},
	closeForm: function () {
		this.setState({
			isFormOpen: false
		});
	},
	render: function () {
		var buttonClass = classNames({
			btn: true,
			'btn-primary': true,
			hide: this.state.isFormOpen
		});
		return (
			<div className="BlueprintCreate">
				<button onClick={this.onNewClick} className={buttonClass}>
					+ New
				</button>
				{this.state.isFormOpen ? <BlueprintCreateForm closeForm={this.closeForm} /> : ''}
			</div>
		);
	}
});

module.exports = Component;
