var React = require('react');
var request = require('common/request');
var classNames = require('classnames');

var Component = React.createClass({
	getInitialState: function() {
		return {
			isNameValid: true,
			isFormValid: false,
		};
	},
	validate: function () {
		// Name input
		var nameEl = React.findDOMNode(this.refs.name);
		var valid = true;

		if (!nameEl.value) {
			this.setState({
				isNameValid: false
			});
			valid = false;
		} else {
			this.setState({
				isNameValid: true
			});
		}
		this.setState({
			isFormValid: valid
	});
		return valid;
	},
	onInputChange: function (e) {
		this.validate();
	},
	onCancelClick: function (e) {
		e.preventDefault();
		this.props.closeForm();
	},
	onCreateClick: function (e) {
		e.preventDefault();
		var name = React.findDOMNode(this.refs.name).value;
		var valid = this.validate();
		if (!valid) {
			return;
		}
		request.post('/api/v1/blueprint', {
			name: name
		}, (err, data) => {
			if (err) {
				console.error(err);
			}
			console.debug('Saved');
			this.props.closeForm();
		});
	},
	render: function () {
		var nameGroupClasses = classNames({
			'form-group': true,
			'has-error': !this.state.isNameValid
		});
		var createButtonClasses = classNames({
			'btn': true,
			'btn-primary': true,
			'disabled': !this.state.isFormValid
		});

		return (
			<div className="BlueprintCreateForm">
				<form>
					<div className={nameGroupClasses}>
						<label className="control-label" for="name">Name</label>
						<input onChange={this.onInputChange} className="form-control" placeholder="Name" type="text" name="name" ref="name"/>
					</div>
					<div className="form-group">
						<button onClick={this.onCreateClick} className={createButtonClasses}>
							Create
						</button>
						<button onClick={this.onCancelClick} className="btn btn-default cancel">
							Cancel
						</button>
					</div>
				</form>
			</div>
		);
	}
});

module.exports = Component;
