'use strict';

// Uncomment the following lines to use the react test utilities
// import React from 'react/addons';
// const TestUtils = React.addons.TestUtils;

import createComponent from 'helpers/createComponent';
import Some from 'components/Some.js';

describe('Some', () => {
    let SomeComponent;

    beforeEach(() => {
        SomeComponent = createComponent(Some);
    });

    it('should have its component name as default className', () => {
        expect(SomeComponent._store.props.className).toBe('Some');
    });
});
