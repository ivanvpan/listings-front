'use strict';

// Uncomment the following lines to use the react test utilities
// import React from 'react/addons';
// const TestUtils = React.addons.TestUtils;

import createComponent from 'helpers/createComponent';
import NavBar from 'components/NavBar.js';

describe('NavBar', () => {
    let NavBarComponent;

    beforeEach(() => {
        NavBarComponent = createComponent(NavBar);
    });

    it('should have its component name as default className', () => {
        expect(NavBarComponent._store.props.className).toBe('NavBar');
    });
});
